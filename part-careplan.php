
<section class="title">
	 <h2 class="headline06">プロのケアマネージャーが<br>最適なプランを作成いたします<span class="line"></span></h2>
	 <p>「創成川イースト　ケアプランセンター」では介護に関する不安・心配を解消・軽減し<br class="pc">
プロのケアマネージャーが、お一人ひとりに最適なプランを作成いたします。</p>
</section>
<!-- title -->

<section class="trouble bg_img1">
	<div class="wrapper960">
		<h2>このような悩みをお抱えになっていませんか？</h2>
		<ul class="cf">
			<li>
				<img src="<?php bloginfo('template_url'); ?>/images/careplan_service_icon1.jpg" alt="高齢や障がいを持ち生活が不安" />
				<h3 class="r2">高齢や障がいを持ち<br>生活が不安</h3>
			</li>
			<li>
				<img src="<?php bloginfo('template_url'); ?>/images/careplan_service_icon2.jpg" alt="身内の介護疲れ" />
				<h3 class="r1">身内の介護疲れ</h3>
			</li>
		</ul>
		<ul class="cf">
			<li>
				<img src="<?php bloginfo('template_url'); ?>/images/careplan_service_icon3.jpg" alt="制度がよくわからない" />
				<h3 class="r1">制度がよくわからない</h3>
			</li>
			<li>
				<img src="<?php bloginfo('template_url'); ?>/images/careplan_service_icon4.jpg" alt="介護保険についてもっと詳しく知りたい" />
				<h3 class="r2">介護保険について<br>もっと詳しく知りたい</h3>
			</li>
		</ul>
		<div class="arrow"><img src="<?php bloginfo('template_url'); ?>/images/careplan_under_arrow.png" /></div>
		<h4>ひとりで悩まないでください！</h3>
		<p>介護の専門スタッフがわかりやすくアドバイスいたします</p>
	</div>
	<!-- wrapper920 -->
</section>
<!-- trouble -->

<section class="region bg_gray">
	<div class="box wrapper">
		<h3>ケアプラン作成 対応地域</h3>
		<p>札幌市中央区・白石区・豊平区・東区・厚別区・江別市・北広島市</p>
	</div>
	<!-- wrapper790 -->
</section>
<!-- region -->

<section class="structure">
	<h3>介護保険の仕組み</h3>
	<p>公的介護保険は40歳以上の人が全員加入して介護保険料を納め、介護が必要になった時に所定の介護サービスが受けられる保険です。65歳以上の人は「第1号被保険者」、40～64歳の人は「第2号被保険者」となります。<br>第1号被保険者は、要介護状態になった原因が何であろうと、公的介護保険のサービスを受けることができますが、第2号被保険者は、老化に起因する特定の病気（16疾患）によって要介護状態になった場合に限り、介護サービスを受けることができます（末期がんも含まれます）。</p>
</section>
<!-- structure -->

<section class="flow bg_beige">
	<h3>公的介護保険を利用するまでの流れ</h3>
	<img src="<?php bloginfo('template_url'); ?>/images/careplan_flow.png" alt="公的介護保険を利用するまでの流れ" />
</section>
<!-- flow -->

<section class="service">
	<h2>公的介護保険から受けられる介護サービス</h2>
	<div class="wrapper">
		<div class="box cf">
			<div class="left">
				<h3>要支援1～要支援2の人</h3>
				<h4>介護予防給付</h4>
				<ul>
					<li>・自宅で生活しながら受けるサービス</li>
					<li>・施設などを利用して受けるサービス</li>
					<li>・介護の環境を整えるためのサービス</li>
				</ul>
			</div>
			<!-- left -->
			<div class="right">
				<h3>要介護1～要介護5の人</h3>
				<h4>介護給付</h4>
				<ul>
					<li>・自宅で生活しながら受けるサービス</li>
					<li>・施設などを利用して受けるサービス</li>
					<li>・介護の環境を整えるためのサービス</li>
					<li>・施設に入所して受けるサービス</li>
				</ul>
			</div>
			<!-- right -->
		</div>
		<!-- box -->
		<p class="linkbtn"><a href="<?php bloginfo('url'); ?>/visitcare/#01">在宅サービスの支給限度額<br class="sp">と利用の目安はこちら</a></p>
	</div>
	<!-- wrapper750 -->
</section>
<!-- service -->

<section id="contact_link">
	<div class="wrapper">
		<h2>指定居宅介護支援事業所</h2>
		<h3><span class="sm">創成川イースト</span>ケアプランセンター</h3>
		<address>札幌市中央区南1条東2丁目マツヒロビル2階</address>
		<div class="box cf">
			<div class="tel"><img src="<?php bloginfo('template_url'); ?>/images/contactlink_tel_icon.png" /><a href="tel:011-251-2292">011-251-2292</a></div>
			<div class="linkbtn"><a href="<?php bloginfo('url'); ?>/contact/?contact_type=創成川イースト　ケアプランセンター"><img src="<?php bloginfo('template_url'); ?>/images/contactlink_mail_icon.png" />お問い合わせフォームはこちら</a></div>
		</div>
		<!-- box -->
	</div>
	<!-- wrapper -->
</section>
<!-- contact_link -->