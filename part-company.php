
<section class="company">
	<h2 class="headline02">会社概要<span class="line"></span></h2>
	<dl>
		<dt>運営会社</dt>
		<dd>キャリアフィットケアサービス株式会社</dd>
	</dl>
	<dl>
		<dt>所在地</dt>
		<dd>〒060-0051<br>札幌市中央区南1条東2丁目3番地2号　マツヒロビル2F<br>TEL : 011-251-2292（代）</dd>
	</dl>
	<dl>
		<dt>代表者</dt>
		<dd>代表取締役社長　村上　真也</dd>
	</dl>
	<dl>
	  <dt>設立</dt>
		<dd>2012年10月</dd>
	</dl>
	<dl>
		<dt>資本金</dt>
		<dd>10,000,000円</dd>
	</dl>
	<dl>
		<dt>事業内容</dt>
		<dd>福祉住宅の運営<br>通所介護・訪問介護・ケアプランセンターの運営<br />総合ヘルスケア・コンサルティング<br />介護用品の販売・販売代理店</dd>
	</dl>
</section>

<section class="careerfit_next">
	<div class="wrapper">
	<img src="<?php bloginfo('template_url'); ?>/images/company_careerfit_next.jpg" alt="CAREERFIT NEXT | アジア共栄事業協同組合札幌支部 | 日本生涯現役推進協議会北海道支部 | 北海道ヘルスケア産業振興協議会" />
	</div>
</section>

<section class="link_bnr">
	<h2 class="headline02">関連会社<span class="line"></span></h2>
	<a href="http://www.careerfit.co.jp/" target="_blank" alt="キャリアフィット株式会社 | お仕事探しのキャリアフィット"><img src="<?php bloginfo('template_url'); ?>/images/link_bnr1.jpg" /></a>
</section>
　