<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo '  '; } ?>
        <?php bloginfo('name'); ?>
    </title>
    <meta name="keyword" content="キャリアフィットケアサービス,北海道北広島,札幌市中央区,訪問介護,デイサービス併設,介護対応型,サービス付き高齢者向け専用住宅,バリアフリー,見学,指定訪問介護事業所"/>
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
    <?php if(is_pc()):?>
    <!--[if lt IE 9]>
    <script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
    <![endif]-->
    <?php endif; ?>

    <?php //以下user設定 ?>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery-3.2.1.min.js"></script>

    <?php if(is_mobile()){ ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/sp.css" type="text/css">
    <?php }else{ ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/pc.css" type="text/css">
    <?php } ?>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/rollover.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/sp_switch.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/flexibility-1.0.6/dist/flexibility.js"></script>

    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slider-pro/dist/css/slider-pro.css">
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slider-pro/dist/js/jquery.sliderPro.js"></script>

    <!-- lightbox -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/lightbox/dist/css/lightbox.min.css">
    <script src="<?php bloginfo('template_url'); ?>/js/lightbox/dist/js/lightbox.min.js"></script>

    <!-- drawer.js -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/drawer/dist/css/drawer.min.css">
    <script src="<?php bloginfo('template_url'); ?>/js/iscroll.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/match-height/dist/jquery.matchHeight.js"></script>
    <script>
    $(function(){
      　　$('.matchHeight').matchHeight();
    });
    </script>    
    <script src="<?php bloginfo('template_url'); ?>/js/drawer/dist/js/drawer.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.drawer').drawer();
    });
    </script>

<script type="text/javascript">
$(function(){
    $('.link').click(function(){
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });
});
</script>

<script>
jQuery(function($) {
    //件名の自動選択
    <?php if ($_GET["contact_type"] != NULL): ?>
        $("input[name='お問い合わせ種別']").val(['<?php echo $_GET['contact_type']; ?>']);
    <?php endif ?>
});
</script>


<script>
    jQuery(function($) {
        if($("#contact_row .error").eq(1).text()=="※電話番号かメールアドレスのどちらかを入力してください。") {
            $("#contact_row .error").eq(1).css("color","#333333");
        }
        if($("#contact_row .error").eq(0).text()=="※電話番号かメールアドレスのどちらかを入力してください。") {
            $("#contact_row .error").eq(0).hide();
        }

    });
</script>


<?php if(is_mobile()): ?>
<script>
    jQuery(function($) {
        console.log("height:" + $("#news").height());
        console.log("width(integer):" + $("#news").width());
    });
</script>
<?php endif; ?>

<?php if(is_pc()): ?>
<script>
  $( document ).ready(function( $ ) {
  $( '#my-slider' ).sliderPro({
	width: "100%",
    height:680,
    fade: true,
    slideAnimationDuration:900,
    autoplayDelay:7000,
	touchSwipe:false,
	smallsize:680,
	arrows: false,
	autoplay: true,
	loop: true,
	visibleSize: '100%',
	responsive:true,
	buttons: false, //下部のドットボタンの有無
	autoScaleLayers:false,//キャプションの自動変形
//	waitForLayers: true,//キャプションのアニメーションが終了してからスライドするか
  });
});
</script>
<?php endif; ?>

<?php if(is_mobile()): ?>
<script>
  $( document ).ready(function( $ ) {
  $( '#my-slider' ).sliderPro({
    width: "100%",
    height: 700,
    aspectRatio: 2,
    arrows: false,
    buttons: false,
    autoplay: true,
    autoHeight: true,
    loop: true,
    visibleSize: '100%',
    forceSize: 'fullWidth',
    responsive:true
  });
});
</script>
<?php endif; ?>

<script type="text/javascript">
  $(function(){
  $("#acMenu dt").on("click", function() {
    $(this).next().slideToggle(); 
    // activeが存在する場合
    if ($(this).children(".accordion_icon").hasClass('active')) {     
      // activeを削除
      $(this).children(".accordion_icon").removeClass('active');        
    }
    else {
      // activeを追加
      $(this).children(".accordion_icon").addClass('active');     
    }     
  });
});
</script>

<?php $user_agent = $_SERVER['HTTP_USER_AGENT'];
if (!strstr($user_agent, 'MSIE')) :?>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
    <script>
    $(function() {
        window.sr = ScrollReveal({ reset: false, mobile: false });
        sr.reveal('.enter-left', { origin: 'left', distance: '50%', duration: 300, scale: 1.0, delay: 300, opacity: 0, });
        sr.reveal('.enter-right', { origin: 'right', distance: '50%', duration: 300, scale: 1.0, delay: 300, opacity: 0, });
        sr.reveal('.enter-top', { origin: 'top', distance: '20%', duration: 500, scale: 1.0, delay: 300, opacity: 0, });
        sr.reveal('.enter-bottom', { origin: 'bottom', distance: '10%', duration: 500, scale: 1.0, delay: 300, opacity: 0, });
        sr.reveal('.rotate', { origin: 'top', distance: '20%', duration: 1200, scale: 1.0, delay: 300, opacity: 0, rotate: { x: 300, y: 300, z: 300 } });
        sr.reveal('.fead', { distance: '0%', duration: 500, scale: 0.1, delay: 300, opacity: 0, });
        sr.reveal('.fead1', { distance: '0%', duration: 500, scale: 1.0, delay: 300, opacity: 0, });
        sr.reveal('.fead2', { distance: '0%', duration: 500, scale: 1.0, delay: 400, opacity: 0, });
        sr.reveal('.fead3', { distance: '0%', duration: 500, scale: 1.0, delay: 500, opacity: 0, });
        sr.reveal('.fead4', { distance: '0%', duration: 500, scale: 1.0, delay: 600, opacity: 0, });
        sr.reveal('.fead5', { distance: '0%', duration: 500, scale: 1.0, delay: 700, opacity: 0, });
        sr.reveal('.fead6', { distance: '0%', duration: 500, scale: 1.0, delay: 800, opacity: 0, });
        sr.reveal('.fead7', { distance: '0%', duration: 500, scale: 1.0, delay: 900, opacity: 0, });
        sr.reveal('.fead8', { distance: '0%', duration: 500, scale: 1.0, delay: 1000, opacity: 0, });
        sr.reveal('.grow1', { distance: '0%', duration: 500, scale: 0.1, delay: 300, opacity: 0, });
        sr.reveal('.grow2', { distance: '0%', duration: 500, scale: 0.1, delay: 400, opacity: 0, });
        sr.reveal('.grow3', { distance: '0%', duration: 500, scale: 0.1, delay: 500, opacity: 0, });
        sr.reveal('.grow4', { distance: '0%', duration: 500, scale: 0.1, delay: 600, opacity: 0, });
        sr.reveal('.grow5', { distance: '0%', duration: 500, scale: 0.1, delay: 700, opacity: 0, });
        sr.reveal('.grow6', { distance: '0%', duration: 500, scale: 0.1, delay: 800, opacity: 0, });
        sr.reveal('.grow7', { distance: '0%', duration: 500, scale: 0.1, delay: 900, opacity: 0, });
        sr.reveal('.grow8', { distance: '0%', duration: 500, scale: 0.1, delay: 1000, opacity: 0, });
        sr.reveal('.fead-right', { origin: 'right', distance: '10%', duration: 500, scale: 1.0, delay: 300, opacity: 0, });
        sr.reveal('.fead-left', { origin: 'left', distance: '10%', duration: 500, scale: 1.0, delay: 300, opacity: 0, });
    });
    </script>
<?php endif; ?>


<script>
  $(function() {
  $('.tab li').click(function() {
    var index = $('.tab li').index(this);
    $('.content li').css('display','none');
    $('.content li').eq(index).css('display','block');
    $('.tab li').removeClass('select');
    $(this).addClass('select');
  });
});
</script>




<?php wp_head(); ?>
</head>
<?php
  $body_id = "";
  $body_class = "";
  if ( is_front_page() ) {
    $body_id = ' id="page_index"';
  }else if ( is_page() ) {
    if($post -> post_parent == 0 ){
        $body_id = ' id="page_'.$post->post_name.'"';
    }else{
        $ancestors =  $post-> ancestors;
        foreach($ancestors as $ancestor){
            $body_id = ' id="page_'.get_post($ancestor)->post_name.'"';
            break;
        }
    }
    $body_class = ' subpage';
    if( $post->post_parent){
        $body_class = ' '.$post->post_name;
    }
  }else if (  is_single() ) {
    $body_id = ' id="page_single"';
   $body_class = " subpage ".get_post_type( $post );
  }else if ( is_archive() ) {
    $body_id = ' id="page_archive"';
    $body_class = " subpage ".get_post_type( $post );
  }else if ( is_404() ) {
    $body_id = ' id="page_404"';
    $body_class = ' subpage';
  }
?>
    <body<?php echo $body_id; ?> class="drawer drawer--right<?php echo $body_class; ?>">
        <div id="outer">

        <header role="banner" class="drawermenu">
            <a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.png" alt="キャリアフィットケアサービス株式会社 | 札幌市中央区"></a>
            <button type="button" class="drawer-toggle drawer-hamburger">
                <span class="sr-only">toggle navigation</span>
                <span class="drawer-hamburger-icon"></span>
            </button>
            <nav class="drawer-nav" role="navigation">
                <ul class="drawer-menu">
                    <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/">トップ</a></li>
                    <li>
                        <dl id="acMenu">
                        <dt>アゼリア館北広島<p class="accordion_icon"><span></span><span></span></p></dt>
                        <dd>
                         <ul class="drawer-menu">
                            <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/house/">アゼリア館北広島トップ</a></li>
                            <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/introduction/">館内のご紹介</a></li>
                            <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/guide/">ご入居案内</a></li>
                            <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/about/">アゼリア館について</a></li>
                            <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/diary/">アゼリア日記（お知らせ）</a></li>
                        </ul>
                        </dd>
                        </dl>
                    </li>
                    <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/visitcare/">創成川イースト<span>ヘルパーステーション</span></a></li>
                    <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/careplan/">創成川イースト<span>ケアプランセンター</span></a></li>
                    <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/healthcare/">総合ヘルスケア</a></li>
                    <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
                    <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
                    <li><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/privacy/">個人情報保護方針</a></li>
                </ul>
            </nav>
        </header>
        <main role="main">
            <header class="global cf">
                <div class="wrapper">
                    <div class="top">
                        <h1><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.png" alt="キャリアフィットケアサービス株式会社 | 札幌市中央区" /></a></h1>
                        <h2>キャリアフィットケアサービス株式会社</h2>
                        <p class="contact"><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/images/contact_icon.svg">お問い合わせ</a></p>
                        <p class="comp"><a href="<?php bloginfo('url'); ?>/company/">会社概要</a></p>
                    </div>
                    <!-- top -->
                </div>
                <!-- wrapper -->
                <div class="wrapper">
                    <nav>
                        <ul class="cf">
                            <li class="s"><a href="<?php bloginfo('url'); ?>/">トップ</a>
                            </li>
                            <li><a class="hover" href="<?php bloginfo('url'); ?>/house/"><span>サービス付き高齢者向け住宅</span>アゼリア館北広島</a>
    						</li>
                            <li><a href="<?php bloginfo('url'); ?>/visitcare/"><span>創成川イースト</span>ヘルパーステーション</a>
                            </li>
                            <li><a href="<?php bloginfo('url'); ?>/careplan/"><span>創成川イースト</span>ケアプランセンター</a>
                            </li>
                            <li class="s"><a href="<?php bloginfo('url'); ?>/healthcare/">総合ヘルスケア</a></li>
                        </ul>
                    </nav>
                </div>
                <!-- wrapper -->
            </header>
            <div id="main_contents" class="cf">