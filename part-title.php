<?php
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
	$category = get_the_category();
	$cat_name = $category[0]->cat_name;

	if($_GET['cat_name']){
	  $cat_name = $_GET['cat_name'];
	}
?>
<div id="pagetitle" class="cf">
	<div class="bg">
	<?php if(is_single() && $post->post_name =="post"):?>
		<h3>お知らせ</h3>
	<?php elseif($post->post_name =='house' || $parent_slug == 'house'): ?> 
		<h3 class="s"><span>サービス付き高齢者向け住宅</span>アゼリア館北広島</h3>
	<?php elseif($post->post_name =='visitcare' || $parent_slug == 'careplan'): ?> 
		<h3 class="s"><span><?php echo $post->post_title;?></span>創成川イースト</h3>
	<?php elseif($post->post_name =='newslist'): ?>
		<h3 class="s"><span><?php echo $cat_name; ?></span>新着情報</h3>
	<?php elseif(is_single() && $post->post_type =="post"): ?>
		<h3 class="s"><span><?php echo $cat_name; ?></span>新着情報</h3>
    <?php elseif(is_404() ):?>
		<h3>準備中</h3>
	<?php else: ?>
		<h3><?php echo $post->post_title;?></h3>
	<?php endif; ?>

	</div>
	<!-- bg -->
</div>
<!-- page_title -->