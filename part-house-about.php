
<?php // 共通CSSクラス　?>
<section class="pagelink_col2">
	<ul class="cf">
		<li><a href="#01">アゼリア館の特長</a></li>
		<li><a href="#02">ロケーション</a></li>
	</ul>
</section>


<section id="01" class="features">
	<h3 class="headline02">アゼリア館の特長<span class="line"></span></h3>
	<h2>アゼリア館北広島の特長をご紹介します。</h2>

	<div class="point cf bg_gray">
		<div class="wrapper">
		<div class="photo">
			<img class="mb15" src="<?php bloginfo('template_url'); ?>/images/	house_about_point1_01.jpg" />
			<img src="<?php bloginfo('template_url'); ?>/images/house_about_point1_02.jpg" />
		</div>
		<!-- photo -->
		<div class="text">
			<h4>point１<div class="underline cf"><div class="left"></div><div class="right"></div></div></h4>
			<h5>ご入居者様個々の生活を大切に。生き甲斐のある毎日をサポート</h5>
			<p>アゼリア館ではご入居者様お一人おひとりがご自宅として過ごしていただける環境づくりを基本方針として運営しております。また、日々の生活のリズム、四季の移り変わりを感じていただけるよう、館内でご提供するサービスや、様々な行事と交流を大切にしています。</p>
		</div>
		<!-- text -->
		</div>
		<!-- wrapper -->
	</div>
	<!-- point -->

	<div class="point cf">
		<div class="wrapper">
		<div class="photo">
			<img src="<?php bloginfo('template_url'); ?>/images/house_about_point2_01.jpg" />
		</div>
		<!-- photo -->
		<div class="text">
			<h4>point2<div class="underline cf"><div class="left"></div><div class="right"></div></div></h4>
			<h5>終身まで暮らせる住宅運営を目指します</h5>
			<p>アゼリア館は自立、要支援の方から要介護3以上の方までご入居が可能な高齢者住宅です。24時間常駐の介護職員と、看護師、栄養士、訪問介護、通所介護が連携して、ご入居者様の日々の健康をサポートします。また、緊急時の対応をはじめ、日常の近隣協力医療機関への受診の支援や日々の訪問診療、訪問看護、訪問薬局との連携により医療面のサポートが必要な方も安心してお住まいいただくことができます。</p>
		</div>
		<!-- text -->
		</div>
		<!-- wrapper -->
	</div>
	<!-- point -->

	<div class="point cf bg_gray">
		<div class="wrapper">
		<div class="photo">
			<img class="mb15" src="<?php bloginfo('template_url'); ?>/images/house_about_point3_01.jpg" />
			<img class="mb15" src="<?php bloginfo('template_url'); ?>/images/house_about_point3_02.jpg" />
			<img src="<?php bloginfo('template_url'); ?>/images/house_about_point3_03.jpg" />
		</div>
		<!-- photo -->
		<div class="text">
			<h4>point3<div class="underline cf"><div class="left"></div><div class="right"></div></div></h4>
			<h5>「アゼリア館北広島」はサービス付き高齢者向け住宅です</h5>
			<p>各居室ともに、法令基準を上回る広い間取りとなっており、Bタイプ、Cタイプはご夫婦での入居のほかにも、ご姉妹、親子など、介護が必要な方とそのご家族といった幅広い生活設計が可能となります。また、高額な一時金、保証金を必要としない賃貸契約方式となっておりますので、ご入居に際する費用を抑えることができます。</p>
		</div>
		<!-- text -->
		</div>
		<!-- wrapper -->
	</div>
	<!-- point -->

	<div class="point cf">
		<div class="wrapper">
		<div class="photo">
			<img class="mb15" src="<?php bloginfo('template_url'); ?>/images/house_about_point4_01.jpg" />
			<img class="mb15" src="<?php bloginfo('template_url'); ?>/images/house_about_point4_02.jpg" />
			<img src="<?php bloginfo('template_url'); ?>/images/house_about_point4_03.jpg" />
		</div>
		<!-- photo -->
		<div class="text">
			<h4>point4<div class="underline cf"><div class="left"></div><div class="right"></div></div></h4>
			<h5>いつまでも健康に暮らしていただくため<br>～訪問介護ステーション・デイサービスセンターが併設</h5>
			<p>訪問介護ステーションではご入居者様の居室に介護職員が訪問し、ケアプランに沿った生活支援（掃除、洗濯など）、身体介護（入浴介護、トイレ介助など）を行います。また、介護保険外の幅広いサービス（お薬の管理、日常範囲外の掃除、通院介助、所用の代行など）からご入居者様ご自身で必要に応じてお選びご利用することができます。</p>
			<p>また隣接するデイサービスセンターは運動による筋肉疲労を軽減する「ソフトリハビリ」を導入し、高齢者のお体の状態に合った無理のないプログラムに基づいて体力の維持、増進を図ります。</p>
			<p>また、プライバシーに配慮した「個室浴」、ご利用者様の感性や感覚を大切にする作業療法、社会参加（外出レク）などをご用意し、ご利用時間は全日型のほか、午前、午後のみの中からご選択いただけます。</p>
		</div>
		<!-- text -->
		</div>
		<!-- wrapper -->
	</div>
	<!-- point -->

	<div class="point cf bg_gray">
		<div class="wrapper">
		<div class="photo">
			<img class="mb15" src="<?php bloginfo('template_url'); ?>/images/house_about_point5_01.jpg" />
			<img class="mb15" src="<?php bloginfo('template_url'); ?>/images/house_about_point5_02.jpg" />
			<img src="<?php bloginfo('template_url'); ?>/images/house_about_point5_03.jpg" />
		</div>
		<!-- photo -->
		<div class="text">
			<h4>point5<div class="underline cf"><div class="left"></div><div class="right"></div></div></h4>
			<h5>笑顔の集うレストラン</h5>
			<h6 class="headline05">毎日の献立は当社専属の栄養士が</h6>
				<ul>
				<li>手作りでご家庭の味に近づける</li>
				<li>健康のためよく噛むお食事作り</li>
				<li>健康食として塩味よりも旨みを重視</li>
				<li>道産米「ななつぼし」使用のこだわり</li>
			</ul>
			<p>こうしたお食事ポリシーに基づいて、栄養バランス、塩分量、アレルギーの有無などを考慮の上献立を作成し、厨房（オープンキッチン）で作り立てをご提供する完全直営方式で運営を行っております。和洋中のほか、季節メニューや全国の名物料理を楽しめる「ご当地メニュー」も好評で、輪厚川を望む明るく開放的なレストラン「つつじ」ではご入居者様同士の会話も弾みます。</p>
		</div>
		<!-- text -->
		</div>
		<!-- wrapper -->
	</div>
	<!-- point -->
</section>
<!-- features -->
<img class="obi" src="<?php bloginfo('template_url'); ?>/images/house_about_obi.jpg" />

<section id="02" class="location">
	<div class="wrapper">
		<h3 class="headline02">ロケーション<span class="line"></span></h3>
		<h2>北広島市は札幌市の南東に隣接し、札幌都市圏のベッドタウンとして<br class="pc">1996年（平成8年）9月に市制施行された街です。</h2>
		<div class="box cf">
			<div class="left">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8255.252571586007!2d141.55675985155005!3d42.98517122839292!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f74d3f459b84e3f%3A0x3a361f762bbb0ac2!2z44CSMDYxLTExMjEg5YyX5rW36YGT5YyX5bqD5bO25biC5Lit5aSu77yU5LiB55uu77yU!5e0!3m2!1sja!2sjp!4v1519115492378" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<!-- left -->
			<div class="right">
				<div class="grid">
					<h4 class="headline05">JR（快速列車）</h4>
					<ul>
						<li>札幌駅～北広島駅　16分</li>
						<li>新札幌駅～北広島駅　7分</li>
						<li>新千歳空港駅～北広島駅　20分</li>
					</ul>
				</div>
				<!-- grid -->

				<div class="grid">
					<h4 class="headline05">バス</h4>
					<ul>
						<li>大谷地ターミナル→北広島駅　32分</li>
						<li>新札幌ターミナル→北広島駅　33分</li>
						<li>北広島駅～アゼリア館（バス「北広島線」ほか）乗車3分<br>「広島市街」停留所から徒歩2分</li>
					</ul>
				</div>
				<!-- grid -->

				<div class="grid">
					<h4 class="headline05">マイカー（高速道路利用の場合）</h4>
					<ul>
						<li>札幌南～（道央自動車道）～北広島　15分</li>
						<li>北広島～（一般道）～アゼリア館　15分</li>
					</ul>
				</div>
				<!-- grid -->

				<div class="grid">
					<h4 class="headline05">マイカー（一般道利用の場合）</h4>
					<ul>
						<li>札幌中心部～（国道274号線／道道46号線）～アゼリア館　45分</li>
					</ul>
				</div>
				<!-- grid -->
			</div>
			<!-- right -->
		</div>
		<!-- box -->
	</div>
	<!-- wrapper -->
</section>

<section class="env">
	<div class="top bg_yellow">
		<div class="wrapper">
			<h3>アゼリア館北広島の周辺環境</h3>
			<p>北広島市のほぼ中心部に位置しています。銀行や買い物などに不自由<br class="pc">しない上、すぐ後ろを流れる輪厚川やレクレーションの森など豊かな<br class="pc">自然に恵まれ、非常に住まいやすい環境です。</p>
		</div>
		<!-- wrapper -->
	</div>
	<!-- top -->

	<div class="bottom bg_gray">
		<div class="wrapper">
			<img src="<?php bloginfo('template_url'); ?>/images/house_about_map.jpg?v=20180621" alt="アゼリア館北広島の周辺環境マップ" />
			<ol class="bg_white cf">
				<div class="left">
					<li>1.　北広島市役所</li>
					<li>2.　コープ/北洋銀行ATM</li>
					<li>3.　ヤマダ電機</li>
					<li>4.　廻転寿司　銀次郎</li>
					<li>5.　輪厚川河川敷</li>
					<li>6.　北広島元町郵便局</li>
					<li>7.　芸術文化ホール</li>
				</div>
				<!-- left -->
				<div class="right">
					<li>8.　JR北広島駅</li>
					<li>9.　ショッピング街</li>
					<li>10.　レクリエーションの森</li>
					<li>11.　北広島総合体育館</li>
					<li>12.　きたひろしま総合運動公園<br>（日本ハムファイターズボールパーク建設予定地）</li>
				</div>
				<!-- right -->
			</ol>
		</div>
		<!-- wrapper -->
	</div>
	<!-- bottom -->
</section>
<!-- env -->




