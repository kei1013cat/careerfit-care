<section id="azeria_contact_link">
	<div class="wrapper">
		<div class="bg_white">
			<h2>アゼリア館北広島</h2>
			<address>〒061-1121　北海道北広島市中央3丁目4－6</address>
		</div>
		<p>ご相談・ご見学のお問い合わせ</p>
		<div class="box cf">
			<div class="tel"><img src="<?php bloginfo('template_url'); ?>/images/contactazeria_tel_icon.png" /><a href="tel:0113722388">011-372-2388</a></div>
			<div class="linkbtn"><a href="<?php bloginfo('url'); ?>/contact/?contact_type=アゼリア館北広島"><img src="<?php bloginfo('template_url'); ?>/images/contactlink_mail_icon.png" />お問い合わせフォームはこちら</a></div>
		</div>
		<!-- box -->
	</div>
	<!-- wrapper -->
</section>
<!-- contact_link -->