
<section class="title">
	<div class="wrapper">
	 <h2 class="headline06">「住み慣れた我が家で暮らしたい」<br>そんな想いを大切にします<span class="line"></span></h2>
	 <p>介護の専門スタッフがご自宅にお伺いして訪問介護サービスをいたします。</p>
	</div>
	<!-- wrapper -->
</section>
<!-- title -->

<section class="service bg_img1">
	<div class="wrapper920">
		<ul class="cf">
			<li class="matchHeight">
				<p><img src="<?php bloginfo('template_url'); ?>/images/visit_service_icon1.png" alt="掃除・洗濯援助" /></p>
				<h3>掃除・洗濯援助</h3>
			</li>
			<li class="matchHeight">
				<p><img src="<?php bloginfo('template_url'); ?>/images/visit_service_icon2.png" alt="身体介護・入浴介助" /></p>
				<h3>身体介護・入浴介助</h3>
			</li>
			<li class="matchHeight">
				<p><img src="<?php bloginfo('template_url'); ?>/images/visit_service_icon3.png" alt="移動や移乗の介助" /></p>
				<h3>移動や移乗の介助</h3>
			</li>
		<?php if(is_pc()): ?></ul><ul class="cf"><?php endif; ?>
			<li class="matchHeight">
				<p><img src="<?php bloginfo('template_url'); ?>/images/visit_service_icon4.png" alt="外出のガイドヘルプ" /></p>
				<h3>外出のガイドヘルプ</h3>
			</li>
			<li class="matchHeight">
				<p><img src="<?php bloginfo('template_url'); ?>/images/visit_service_icon5.png" alt="病院受診や入院の付添" /></p>
				<h3>病院受診や入院の付添</h3>
			</li>
			<li class="matchHeight">
				<p><img src="<?php bloginfo('template_url'); ?>/images/visit_service_icon6.png" alt="調理援助" /></p>
				<h3>調理援助</h3>
			</li>
		</ul>
	</div>
	<!-- wrapper920 -->
</section>
<!-- service -->

<section class="introduction">
	<div class="wrapper920">
	<div class="grid cf">
		<div class="photo">
			<img src="<?php bloginfo('template_url'); ?>/images/visit_introduction_photo1.jpg" />
		</div>
		<div class="text">
			<h3 class="headline07">身体介護<span class="sm">（保険、自費）</span></h3>
			<p>お身体の状態やご希望に合わせて、介護サービスをご提供いたします。</p>
			<ul>
				<li>・身体介護</li>
				<li>・入浴介助</li>
				<li>・移動や移乗の介助</li>
				<li>・外出のガイドヘルプ</li>
				<li>・病院受診や入院の付添</li>
			</ul>
		</div>
	</div>
	<!-- grid -->
	<div class="grid cf">
		<div class="photo">
			<img src="<?php bloginfo('template_url'); ?>/images/visit_introduction_photo2.jpg?v=20180621" />
		</div>
		<div class="text">
			<h3 class="headline07">生活援助<span class="sm">（保険、自費）</span></h3>
			<p>お身体の状態やご希望に合わせて、介護サービスをご提供いたします。</p>
			<ul>
				<li>・掃除</li>
				<li>・洗濯</li>
				<li>・調理援助</li>
			</ul>
		</div>
	</div>
	<!-- grid -->
	</div>
	<!-- wrapper -->
</section>
<!-- introduction -->

<section class="use bg_beige" id="01">
	<div class="wrapper">
	<h3>在宅サービスの支給限度額と利用の目安</h3>
	<div class="sp"><p class="kome">左右のスワイプで料金が確認できます。</p></div>
	<div class="scroll">
	<table>
	  <tr>
	    <th class="c1">区分</th>
	    <th class="c2">要介護・要支援認定の目安</th>
	    <th class="c3">サービス利用例</th>
	    <th class="c4">１ヶ月の<br class="pc" />
	      利用限度額</th>
	    <th class="c5">１ヶ月の利用限度額まで<br class="pc" />
	      利用した場合の自己負担額</th>
	  </tr>
	  <tr>
	    <th class="row2">要支援１</th>
	    <td><h4>日常生活の一部について介助を必要とする状態</h4>
	      入浴や掃除など、日常生活の一部に見守りや手助けが必要</td>
	    <td><h5>週２~３回のサービス</h5>
	      週１回の介護予防訪問介護<br />
	      介護予防通所系サービス</td>
	    <td class="en">50,030円</td>
	    <td class="jiko-en">5,003円</td>
	  </tr>
	  <tr>
	    <th class="row2">要支援2</th>
	    <td rowspan="2"><h4>生活の一部について部分的に介護を必要とする状態</h4>
	      食事や排泄など、時々介護が必要。立ち上がりや歩行などに不安定さがみられる事が多い。この状態のうち、介護予防サービスにより状態の維持や改善が見込まれる方は要支援２。</td>
	    <td><h5>週3~4回のサービス</h5>
	      週2回の介護予防訪問介護<br />
	      介護予防通所系サービス</td>
	    <td class="en">104,730円</td>
	    <td class="jiko-en">10,473円</td>
	  </tr>
	  <tr>
	    <th class="row3">要介護1</th>
	    <td><h5>1日1回程度のサービス</h5>
	      週３回の訪問介護<br />
	      週１回の訪問看護<br />
	      週2回の通所系サービス</td>
	    <td class="en">166,920円</td>
	    <td class="jiko-en">16,692円</td>
	  </tr>
	  <tr>
	    <th class="row4">要介護2</th>
	    <td><h4>軽度の介護を必要とする状態</h4>
	      食事や排泄に何らかの介助が必要。立ち上がりや歩行などに何らかの支えが必要。</td>
	    <td><h5>1日１~2回程度のサービス</h5>
	      週１回の介護予防訪問介護<br />
	      介護予防通所系サービス</td>
	    <td class="en">196,160円</td>
	    <td class="jiko-en">19,616円</td>
	  </tr>
	  <tr>
	    <th class="row5">要介護3</th>
	    <td><h4>中程度の介護を必要とする状態</h4>
	      食事や排泄に一部介助が必要。入浴などに全面的に介助が必要。片足での立位保持が出来ない。</td>
	    <td><h5>1日2回程度のサービス</h5>
	      週２回の訪問介護<br />
	      週１回の訪問看護<br />
	      毎日1回、夜間巡回型訪問介護</td>
	    <td class="en">269,310円</td>
	    <td class="jiko-en">26,931円</td>
	  </tr>
	  <tr>
	    <th class="row6">要介護4</th>
	    <td><h4>重度の介護を必要とする状態</h4>
	      食事に一部介護が必要。排泄・入浴などに全面的な介助が必要。両足での立位保持がほとんど出来ない。</td>
	    <td><h5>1日2~３回程度のサービス</h5>
	      週6回の訪問介護<br />
	      週2回の訪問看護<br />
	      毎日1回、夜間巡回型訪問介護</td>
	    <td class="en">308,060円</td>
	    <td class="jiko-en">30,806円</td>
	  </tr>
	  <tr>
	    <th class="row7">要介護5</th>
	    <td><h4>最重度の介護を必要とする状態</h4>
	      日常生活を遂行する能力は著しく低下し、日常生活全般に介護が必要。意志の伝達がほとんど出来ない。</td>
	    <td><h5>1日3~4回程度のサービス</h5>
	      週5回の訪問介護<br />
	      週2回の訪問看護<br />
	      毎日2回、早朝・夜間巡回型訪問介護</td>
	    <td class="en">360,650円</td>
	    <td class="jiko-en">36,065円</td>
	  </tr>
	</table>
	</div>
	<p>2015年8月から、65歳以上で合計所得金額が160万円（単身で年金収入のみで280万円）以上の人は自己負担額が1割から2割へ引き上げられました。2割負担となるのは基準以上の所得のある本人のみです。</p>
	</div>
	<!-- wrapper -->
</section>

<section id="contact_link">
	<div class="wrapper">
		<h2>指定訪問介護事業所</h2>
		<h3><span class="sm">創成川イースト</span>ヘルパーステーション</h3>
		<address>札幌市中央区南1条東2丁目マツヒロビル2階</address>
		<div class="box cf">
			<div class="tel"><img src="<?php bloginfo('template_url'); ?>/images/contactlink_tel_icon.png" /><a href="tel:011-251-2292">011-251-2292</a></div>
			<div class="linkbtn"><a href="<?php bloginfo('url'); ?>/contact/?contact_type=創成川イースト ヘルパーステーション"><img src="<?php bloginfo('template_url'); ?>/images/contactlink_mail_icon.png" />お問い合わせフォームはこちら</a></div>
		</div>
		<!-- box -->
	</div>
	<!-- wrapper -->
</section>
<!-- contact_link -->