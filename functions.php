<?php

//-------------------------------------------
// 使用しないメニューを非表示にする
//-------------------------------------------
function remove_admin_menus() {
 
    // level10以外のユーザーの場合
//    if (!current_user_can('level_10')) {
        global $menu;

        // unsetで非表示にするメニューを指定
//        unset($menu[2]);        // ダッシュボード
//      unset($menu[5]);        // 投稿
//        unset($menu[10]);       // メディア
//        unset($menu[20]);       // 固定ページ
        unset($menu[25]);       // コメント
//        unset($menu[60]);       // 外観
//        unset($menu[65]);       // プラグイン
//        unset($menu[70]);       // ユーザー
//        unset($menu[75]);       // ツール
//        unset($menu[80]);       // 設定
//    }
}
add_action('admin_menu', 'remove_admin_menus');



//-------------------------------------------
// スマホならtrue, タブレット・PCならfalseを返す
//-------------------------------------------

global $is_mobile;
$is_mobile = false;

$useragents = array(
 'iPhone',          // iPhone
 'iPod',            // iPod touch
 'Android',         // 1.5+ Android
 'dream',           // Pre 1.5 Android
 'CUPCAKE',         // 1.5+ Android
 'blackberry9500',  // Storm
 'blackberry9530',  // Storm
 'blackberry9520',  // Storm v2
 'blackberry9550',  // Storm v2
 'blackberry9800',  // Torch
 'webOS',           // Palm Pre Experimental
 'incognito',       // Other iPhone browser
 'webmate'          // Other iPhone browser
 );
 $pattern = '/'.implode('|', $useragents).'/i';
 if( preg_match($pattern, $_SERVER['HTTP_USER_AGENT'])){
   $is_mobile = preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
 }

function is_mobile(){
  global $is_mobile;
  return $is_mobile;
  //return false;
}
function is_pc(){
  if(is_mobile()){
    return false;
  }else{
    return true;
  }
}
function mobile_img(){
  if (is_mobile()) {
    echo "_sp";
  }
}

//-------------------------------------------
// 固定ページでPHPをinclude
//-------------------------------------------
function Include_php($params = array()) {
    extract(shortcode_atts(array(
        'file' => 'default'
    ), $params));
    ob_start();
    include(get_theme_root() . '/' . get_template() . "/$file.php");
    return ob_get_clean();
}
 
add_shortcode('includephp', 'Include_php');


//-------------------------------------------
//プレビューボタン非表示
//-------------------------------------------
add_action('admin_print_styles', 'admin_preview_css_custom');
function admin_preview_css_custom() {
   echo '<style>#preview-action {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
   echo '<style>#message a {display: none;}</style>';
}

//-------------------------------------------
//記事画像パスを相対パスで利用する
//-------------------------------------------

function imagepassshort($arg) {
$content = str_replace('"images/', '"' . get_bloginfo('template_directory') . '/images/', $arg);
return $content;
}
add_action('the_content', 'imagepassshort');

//-------------------------------------------
//　投稿の自動整形を無効
//-------------------------------------------
//出力側の変換機能を無効化
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_content', 'wptexturize' );

// ビジュアルエディタの変換も無効
function override_mce_options( $init_array ) {
    global $allowedposttags;

    $init_array['valid_elements']          = '*[*]';
    $init_array['extended_valid_elements'] = '*[*]';
    $init_array['valid_children']          = '+a[' . implode( '|', array_keys( $allowedposttags ) ) . ']';
    $init_array['indent']                  = true;
    $init_array['wpautop']                 = false;
    $init_array['force_p_newlines']        = false;

    return $init_array;
}

add_filter( 'tiny_mce_before_init', 'override_mce_options' );

//-------------------------------------------
// エディタをビジュアルにする
//-------------------------------------------
//add_filter('wp_default_editor', create_function('', 'return "tinymce";'));

//-------------------------------------------
//管理画面の「見出し１」等を削除する
//-------------------------------------------

function custom_editor_settings( $initArray ){
$initArray['block_formats'] = "段落=p; 見出し1=h3; 見出し2=h4;";
return $initArray;
}
add_filter( 'tiny_mce_before_init', 'custom_editor_settings' );


//-------------------------------------------
//ページャー
//-------------------------------------------

function bmPageNaviGallery() {
  global $wp_rewrite;
  global $wp_query;
  global $paged;
 
  $paginate_base = get_pagenum_link(1);
  if (strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()) {
    $paginate_format = '';
    $paginate_base = add_query_arg('page', '%#%');
  } else {
    $paginate_format = (substr($paginate_base, -1 ,1) == '/' ? '' : '') .
    untrailingslashit('page/%#%', 'paged');
    $paginate_base .= '%_%';  
  }
  $cat = "";
  if(!empty($_GET['cat'])){
  $cat = "&cat=".htmlspecialchars($_GET['cat'], ENT_QUOTES);
  }
  //$paginate_base = "./page/%#%".$cat;
  $result = paginate_links( array(
   // 'base' => $paginate_base,
    'format' => $paginate_format,
    'total' => $wp_query->max_num_pages,
    'mid_size' => 4,
    'prev_text' => '&lt;&lt;',
    'next_text' => '&gt;&gt;',
    'current' => ($paged ? $paged : 1),
  ));
 
  return $result;
}



/* MW WP From 自作バリデーション */
 
function mwform_validation_rule_test( $validation_rules ) {
  if ( ! class_exists("MW_Validation_Rule_Test") ) {
    class MW_Validation_Rule_Test extends MW_WP_Form_Abstract_Validation_Rule {
      /**
       * バリデーションルール名を指定
       *
       * @var string
       */
      protected $name = 'カナ';
      /**
       * バリデーションチェック
       *
       * @param string $key name属性
       * @param array  $option
       *
       * @return string エラーメッセージ
       */
      public function rule( $key, array $options = array() ) {

          $tel_value = $this->Data->get( "電話番号" );
          $mail_value = $this->Data->get( "メールアドレス" );
          if (MWF_Functions::is_empty( $tel_value ) && MWF_Functions::is_empty( $mail_value ) ) {
                $defaults = array(
                    'message' => __( '※電話番号かメールアドレスのどちらかを入力してください。'.$tel_value.$mail_value , 'mw-wp-form' ),
                );
                $options  = array_merge( $defaults, $options );
                return $options['message'];
          }
      }
      /**
       * 設定パネルに追加
       *
       * @param int   $key   バリデーションルールセットの識別番号
       * @param array $value バリデーションルールセットの内容
       */
      public function admin( $key, $value ) {
        ?>
        <label>
          <input type="checkbox" <?php checked( $value[ $this->getName() ], 1 ); ?> name="<?php echo MWF_Config::NAME; ?>[validation][<?php echo $key; ?>][<?php echo esc_attr( $this->getName() ); ?>]" value="1" />
          <?php esc_html_e( 'メールアドレス or 電話番号の必須', 'mw-wp-form' ); ?>
        </label>
        <?php
      }
    }
  }
  $instance = new MW_Validation_Rule_Test();
  $validation_rules[$instance->getName()] = $instance;
  return $validation_rules;
}
add_filter( 'mwform_validation_rules', 'mwform_validation_rule_test' );


?>