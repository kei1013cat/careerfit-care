<section class="link">
  <div class="wrapper">
    <h2 class="headline01">関連リンク<span  class="line"></span></h2>
    <div class="bg_white">
      <ul class="cf">
        <li><a href="<?php bloginfo('url'); ?>/overview/">会社概要</a></li>
        <li><a href="<?php bloginfo('url'); ?>/overview/coporate/">会社沿革</a></li>

      </ul>
      <ul class="cf">
        <li><a href="<?php bloginfo('url'); ?>/overview/trading/">主要取引先</a></li>
        <li><a href="<?php bloginfo('url'); ?>/overview/nature/">環境への取り組み</a></li>

      </ul>
      <ul class="cf">
        <li><a href="<?php bloginfo('url'); ?>/overview/privacy/">プライバシーポリシー</a></li>
      </ul>

    </div>
    <!-- bg_white -->
  </div>
  <!-- wrapper -->
</section>