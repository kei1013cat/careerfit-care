<div id="sidebar">

<div class="menu">
<?php
  $category = get_the_category();
  $cat_name = $category[0]->cat_name;
?>
  <h3>最新の記事（<?php echo  $cat_name; ?>）</h3>
  <ul class="cf">
    <?php
		$wp_query = new WP_Query();
		$param = array(
			'posts_per_page' => '10', //表示件数。-1なら全件表示
			'post_status' => 'publish',
			'orderby' => 'date', //ID順に並び替え
      'category_name' => $cat_name, //特定のカテゴリースラッグを指定
			'order' => 'DESC'
		);
		$wp_query->query($param);?>
    <?php if($wp_query->have_posts()): while($wp_query->have_posts()) : $wp_query->the_post(); ?>
    <li class="newslist"> <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
      <?php $title= mb_substr($post->post_title,0,30); echo $title;?>
      </a></li>
    <?php endwhile; ?>
    <?php endif; ?>
  </ul>
  <p class="linkbtn1"><a href="<?php bloginfo('url'); ?>/newslist/?cat_name=<?php echo $cat_name; ?>">一覧へ</a></p>

  <?php wp_reset_query(); ?>
</div><!-- menu -->  
  
  
</div>
<!-- sidebar -->