<?php get_header(); ?>

<?php
    $parent_id = $post->post_parent; // 親ページのIDを取得
    $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
?>

<?php if(($post->post_name =="house" || $parent_slug == "house") && is_pc()):?>
<?php include (TEMPLATEPATH.'/house_kita_menulink.php'); ?>
<?php endif; ?>

<?php include (TEMPLATEPATH . '/part-title.php'); ?>

<div id="contents">
    <?php 
    $file_name = "";
    if($post -> post_parent != 0 ){
//        $ancestors = array_reverse( $post-> ancestors );
        $post_id_arr = array_reverse(get_post_ancestors($post->ID));
        foreach($post_id_arr as $p_id){
            $file_name .= get_post( $p_id )->post_name.'-';
        }
    }

    $file_name = TEMPLATEPATH.'/part-'.$file_name.$post->post_name.'.php';

    if (file_exists($file_name)) {
      include ($file_name);


    }?>

<?php if(($post->post_name =="house" || $parent_slug == "house") && is_mobile()):?>
<?php include (TEMPLATEPATH.'/house_kita_menulink.php'); ?>
<?php endif; ?>

    <?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
    <?php the_content(); ?>
    <?php endwhile; ?>
    <?php else : ?>
    <?php include (TEMPLATEPATH . '/404.php'); ?>
    <?php endif; ?>

<?php if(($post->post_name =="house" || $parent_slug == "house")):?>
<?php include (TEMPLATEPATH.'/azeria_contact_link.php'); ?>
<?php endif; ?>




</div>





<!-- contents -->
<?php get_footer(); ?>