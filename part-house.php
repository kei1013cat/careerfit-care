<section class="house">
	<div class="wrapper">
		<h2 class="headline04">やすらぎと彩りあふれる日々</h2>
		<div class="grid">
			<div class="photo">
				<img src="<?php bloginfo('template_url'); ?>/images/house_photo1.jpg?v=20180621" />
			</div>
			<!-- photo -->
			<div class="text">
				<h3>ご入居者さま一人ひとりが主役です</h3>
				<p>アゼリア館は北海道北広島市にある介護対応型サービス付き高齢者向け住宅です。 札幌からJRで16分、新さっぽろから7分という快適なアクセス。輪厚川のせせらぎと小鳥のさえずりが聞こえる、自然とやすらぎにあふれた住まい。</p>
				<p>専門スタッフが24時間、日中は看護師が常駐し近隣の医療機関と連携することで、要介護3以上の方でも安心していつまでもご入居いただけるよう、健康面、生活面のサポートを行います。</p>
				<p>今までの生活習慣と今後の自立支援を大切にし、掃除・洗濯・買い物代行など、ご希望のサービスはご入居者さまに合せてお選びいただけます。</p>
				<p>「安心・安全」はもちろん、お一人ひとりが主役となってお過ごしいただける「自由と選択」のある毎日。そしてご入居者様だけでなくご家族にもやすらぎを感じていただく、それがアゼリア館のサービスのコンセプトです。</p>
			</div>
			<!-- text -->
		</div>
		<!-- grid -->
	</div>
	<!-- wrapper -->
</section>
<!-- top -->

<section class="support">
	<div class="wrapper880">
		<h2 class="headline02">広々とした居室空間と安心のサポート体制</h2>
		<ul>
			<li>共に要介護のご夫婦まで、しっかりサポート。安心してご入居いただけます。</li>
			<li>併設の介護ステーションスタッフが24時間常駐。また平日日中は看護師も常駐しています。</li>
			<li>全室広々として収納もたっぷり。使い慣れた愛用品を持ち込んでのご入居も歓迎しています。</li>
		</ul>
		<div class="box cf">
			<div class="left"><img src="<?php bloginfo('template_url'); ?>/images/house_comp_photo1.jpg" /></div>
			<div class="right"><img src="<?php bloginfo('template_url'); ?>/images/house_comp_photo2.jpg" /></div>
		</div>
		<!-- box -->
	</div>
	<!-- wrapper -->
</section>

<!-- お知らせ -->



<?php require_once('rss_fetch.inc'); ?>
<?php
$url = 'http://azaleakan.jugem.jp/?mode=rss';
$rss = fetch_feed($url);

if (!is_wp_error( $rss ) ) : // ちゃんとフィードが生成されているかをチェックします。
    // すべてのフィードから最新５件を出力します。
    $maxitems = $rss->get_item_quantity(5); 
    // 0件から始めて指定した件数までの配列を生成します。
    $rss_items = $rss->get_items(0, $maxitems); 
endif;
?>
<section class="news bg_yellow">
	<div class="wrapper">
	  <h3>アゼリア日記<span class="small">（お知らせ）</span></h3>

        <?php
        foreach ( $rss_items as $item ) : 
       	?>
        <?php $f_date = $item->get_date('Y年m月d日'); ?>

	    <dl class="newslist cf">
          <dt><?php echo $f_date; ?></dt>
          <dd>
            <a href='<?php echo $item->get_permalink(); ?>'
            target="_blank" title='<?php echo 'Posted '.$item->get_date('j F Y | g:i a'); ?>'>
            <?php echo $item->get_title(); ?>
            </a>
          </dd>
	    </dl>
        <?php endforeach; ?>
	  <p class="linkbtn1"><a href="<?php bloginfo('url'); ?>/house/diary/">記事一覧へ<span class="icon"></span></a></p>
	</div>
	<!-- wrapper -->
</section>
<!-- news -->
