</div>
<!-- contents -->
<footer>
  <div class="wrapper cf">
    <div class="comp">
      <h2><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/footer_logo.png" alt="キャリアフィットケアサービス株式会社 | 札幌市中央区"></a></h2>
      <address>〒060-0051<br>札幌市中央区南１条東２丁目3番地２号<br>マツヒロビル2F</address>
      <p>TEL ： <a href="tel:0112512292">011-251-2292</a></p>

    </div>
    <!-- comp -->
    <nav class="box">
      <ul class="left cf">
        <li class="root"><a href="<?php bloginfo('url'); ?>/">トップページ</a></li>
        <li class="root"><a href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
        <li class="root"><a href="<?php bloginfo('url'); ?>/house/">サービス付き高齢者向け住宅<br />アゼリア館北広島</a></li>
        <li class="pc">
          <ol>
            <li><a href="<?php bloginfo('url'); ?>/house/introduction/">館内のご紹介</a></li>
            <li><a href="<?php bloginfo('url'); ?>/house/guide/">ご入居案内</a></li>
            <li><a href="<?php bloginfo('url'); ?>/house/about/">アゼリア館の特長・ロケーション</a></li>
            <li><a href="<?php bloginfo('url'); ?>/house/diary/">アゼリア日記（お知らせ）</a></li>
          </ol>
        </li>
      </ul>
      <!-- left -->

      <ul class="right">
        <li class="root"><a href="<?php bloginfo('url'); ?>/visitcare/">創成川イースト　ヘルパーステーション</a></li>
        <li class="root"><a href="<?php bloginfo('url'); ?>/careplan/">創成川イースト　ケアプランセンター</a></li>
        <li class="root"><a href="<?php bloginfo('url'); ?>/healthcare/">総合ヘルスケア</a></li>
        <li>
          <ol>
            <li><a href="<?php bloginfo('url'); ?>/healthcare/">シニア、ご家族の皆様へ</a></li>
            <li><a href="<?php bloginfo('url'); ?>/healthcare/">企業のご担当者様へ</a></li>
          </ol>
        </li>
        <li class="root"><a href="<?php bloginfo('url'); ?>/privacy/">個人情報保護方針</a></li>
        <li class="root"><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
      </ul>
      <!-- right -->
    </nav>
  </div>
  <!-- wrapper -->
  <div class="copy">
    <p>Copyright &copy; 2013 キャリアフィットケアサービス Rights Reserved.</p>
  </div>
  <!-- copy -->
</footer>
<p id="page_top"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/pagetop.svg" alt="pagetop"></a></p>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
<?php wp_footer(); ?>
</main>
</div><!--outer -->
</body>

</html>