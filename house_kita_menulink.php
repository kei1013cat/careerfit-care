<section id="house_menulink">
  <div class="wrapper">
    <h2 class="sp">アゼリア館北広島</h2>
    <ul class="cf">

        <li>
        <a <?php if($post->post_name =='house'): ?> class="hover" <?php endif; ?> href="<?php bloginfo('url'); ?>/house/">
            アゼリア館北広島トップ
        </a></li>
        <li>
        <a <?php if($post->post_name =='introduction'): ?> class="hover" <?php endif; ?> href="<?php bloginfo('url'); ?>/introduction/">
            館内のご紹介
        </a></li>
        <li>
        <a <?php if($post->post_name =='guide'): ?> class="hover" <?php endif; ?> href="<?php bloginfo('url'); ?>/guide/">
            ご入居案内
        </a></li>
        <li>
        <a <?php if($post->post_name =='about'): ?> class="hover" <?php endif; ?> href="<?php bloginfo('url'); ?>/about/">
            アゼリア館について
        </a></li>
        <li>
        <a <?php if($post->post_name =='diary'): ?> class="hover" <?php endif; ?> href="<?php bloginfo('url'); ?>/diary/">
            アゼリア日記（お知らせ）
        </a></li>

    </ul>

  </div>
  <!-- wrapper -->
</section>