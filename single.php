<?php get_header(); ?>
<?php include (TEMPLATEPATH . '/part-title.php'); ?>
      <?php include (TEMPLATEPATH . '/part-pan.php'); ?>
<div class="wrapper cf">
	<div id="contents" class="col2">


		<section class="entry_post">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article <?php post_class(); ?>>
				<div class="entry_header">
					<p>
						<time class="entry-date" datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate="<?php the_time( 'Y-m-d' ); ?>">
							<?php the_time( 'Y.m.d'  ); ?>
						</time>
					</p>
					<h3 class="entry-title">
						<?php the_title(); ?>
					</h3>
				</div>
				<section class="entry_content">
					<?php the_content(); ?>
				</section>
				<ul class="page_link cf">
					<li class="prev">
						<?php previous_post_link('%link', '« 前の記事へ', false); ?>
					</li>
					<li class="next">
						<?php next_post_link('%link', '次の記事へ »', false); ?>
					</li>
				</ul>
			</article>
			<?php endwhile; endif; ?>
			<?php wp_reset_query(); ?>
		</section>
	</div>
	<!-- contents -->
	<div class="sidebar">
		<?php get_sidebar(); ?>
	</div>
	<!-- sidebar -->
</div>
<!-- wrapper -->
<?php get_footer(); ?>
