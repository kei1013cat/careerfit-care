<?php include (TEMPLATEPATH . '/part-title.php'); ?>
  <div class="bg_gray">
  <?php include (TEMPLATEPATH . '/part-pan.php'); ?>
  <h2 class="headline01">お知らせ<span class="line"></span></h2>
  <div class="wrapper bg_white">
  <?php
				$paged = (int) get_query_var('paged');
		$wp_query = new WP_Query();
		$param = array(
			'post_status' => 'publish',
			'orderby' => 'date',
			'paged' => $paged,
			'order' => 'DESC'
		);
		$wp_query->query($param);?>
  <?php if ( have_posts() ) :?>
  <section class="news">
    <?php while ( have_posts() ) : the_post(); ?>
      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
    <dl class="cf">
      <dt>
        <?php the_time('Y.m.d'); ?>
        </dt>
      <dd><?php echo $post->post_title;?></dd>
    </dl>
  </a>
    <?php endwhile; ?>
  </section>
  <div class="pagination"> <?php echo bmPageNaviGallery(); // ページネーション出力 ?> </div>
  <!-- pagination -->
  <?php else : ?>
  記事が見つかりません。
  <?php endif; ?>
  <?php wp_reset_query(); ?>
  </div>
  <!-- wrapper -->

</div>
<!-- bg_gray -->