<section class="bottom_link">
  <div class="wrapper">

  <?php if($post->post_name =='stress'): ?>
  	<p class="msg">事前準備からサポート</p>
    <p class="linkbtn aboutbtn"><a href="<?php bloginfo('url'); ?>/about/">こころメイトのご紹介<span class="icon"></span></a></p>
    <p class="linkbtn downloadbtn"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/icon_download.svg">カタログダウンロード<span class="icon"></span></a></p>
  <?php endif; ?>

  <?php if($post->post_name =='about' || $post->post_name =='feature' || $post->post_name =='glossary' || $post->post_name =='qa'): ?>
    <p class="linkbtn downloadbtn"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/icon_download.svg">カタログダウンロード<span class="icon"></span></a></p>
    <p class="linkbtn contactbtn"><a href="#">お問い合わせはこちら<span class="icon"></span></a></p>
  <?php endif; ?>

  <?php if($post->post_name =='contact'): ?>
    <p class="linkbtn downloadbtn"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/icon_download.svg">カタログダウンロード<span class="icon"></span></a></p>
  <?php endif; ?>
  </div>
  <!-- wrapper -->
</section>