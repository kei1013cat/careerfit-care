<?php // 共通CSSクラス　?>
<section class="pagelink_col3">
    <ul class="cf">
        <li><a href="#01">ご入居条件</a></li>
        <li><a href="#02">ご入居・生活に必要な費用</a></li>
        <li><a href="#03">介護対応について</a></li>
    </ul>
</section>

<section id="require" class="require" id="01">
    <div class="wrapper">
        <h3 class="headline02">ご入居条件<span class="line"></span></h3>
        <div class="grid">
            <h3 class="headline05">ご入居いただける方</h3>
            <ul>
                <li>60歳以上、原則要介護1以上の方</li>
                <li>介護保険法に規程する要介護認定を受けている60歳未満の方</li>
            </ul>
        </div>
        <!--grid -->

        <div class="grid">
            <h3 class="headline05">同居いただける方</h3>
            <ul>
                <li>配偶者（届け出はしていないが事実上の夫婦と同様の関係にある方も含みます）</li>
                <li>60歳以上のご親族</li>
                <li>要介護認定または要支援認定を受けている60歳未満のご親族</li>
                <li>入居者の介護が目的で同居が必要な方</li>
            </ul>
        </div>
        <!--grid -->
    </div>
    <!-- wrapper -->
</section>
<!-- require -->

<section class="cost bg_gray" id="02">
    <div class="wrapper">
        <h3 class="headline02">ご入居・生活に必要な費用<span class="line"></span></h3>
        <div class="grid">
            <h4 class="headline05">ご入居一時金</h4>
            <table class="style01">
                <col span="3">
                <tr>
                    <th>項目</th>
                    <th>Aタイプ</th>
                    <th>Bタイプ（一人）</th>
                    <th>Bタイプ（二人）</th>
                    <th>Cタイプ（二人）</th>
                </tr>
                <tr>
                    <th>敷金（2ヶ月分）</th>
                    <td>120,000</td>
                    <td>160,000</td>
                    <td>160,000</td>
                    <td>240,000</td>
                </tr>
            </table>
            <p class="kome">※別途、火災保険料（15,000～20,000円前後）がかかります。</p>
        </div>
        <!-- gird -->

        <div class="grid">
            <h4 class="headline05">月額ご利用料金［前家賃方式］</h4>
            <table class="style01">
                <col>
                <col>
                <col span="3">
                <tr>
                    <th>項目</th>
                    <th>Aタイプ</th>
                    <th>Bタイプ（一人）</th>
                    <th>Bタイプ（二人）</th>
                    <th>Cタイプ（二人）</th>
                </tr>
                <tr>
                    <th>家賃</th>
                    <td>60,000</td>
                    <td>80,000</td>
                    <td>80,000</td>
                    <td>120,000</td>
                </tr>
                <tr>
                    <th>共益費</th>
                    <td>20,000</td>
                    <td>20,000</td>
                    <td>30,000</td>
                    <td>30,000</td>
                </tr>

                <tr>
                    <th>冬期維持費</th>
                    <td>5,000</td>
                    <td>5,000</td>
                    <td>5,000</td>
                    <td>5,000</td>
                </tr>
                <tr>
                    <th>介護管理費</th>
                    <td>16,200</td>
                    <td>16,200</td>
                    <td>32,400</td>
                    <td>32,400</td>
                </tr>


                <tr>
                    <th>厨房管理費</th>
                    <td>23,400</td>
                    <td>23,400</td>
                    <td>46,800</td>
                    <td>46,800</td>
                </tr>
                <tr>
                    <th>食費（30日分）</th>
                    <td>30,000</td>
                    <td>30,000</td>
                    <td>60,000</td>
                    <td>60,000</td>
                </tr>
                <tr>
                    <th>合計</th>
                    <td>154,600</td>
                    <td>174,600</td>
                    <td>254,200</td>
                    <td>294,200</td>
                </tr>
            </table>

            <ul>
                <li>Bタイプ南向きは月額家賃90,000円となります。</li>
                <li>ご入居時に敷金として家賃の2ヵ月分をお預かりいたします。</li>
                <li>ご入居時、火災保険料2年分が別途かかります。</li>
                <li>冬期維持費は12月から3月の間、ご請求申し上げます。</li>
                <li>介護管理費には介護保険自己負担金は含まれません。</li>
                <li>水道光熱費は、各供給元との直接契約による実費請求となります。</li>
                <li>駐車場あり（月3,000円/台、税別）</li>
                <li>上記金額には消費税8%を含んでおります。</li>
            </ul>
            <h4>ご入居に関する様々なご相談を承ります。お気軽にお問合せください。</h4>
        </div>
        <!-- grid -->
    </div>
    <!-- wrapper -->

</section>
<!-- cost -->


<section class="about" id="03">
    <div class="wrapper">
        <h3 class="headline02">介護管理費に含まれる介護対応について<span class="line"></span></h3>
        <h4 class="headline05">介護管理費に含まれる項目</h4>

        <dl>
            <dt><span class="pink">1.</span>入退去の対応</dt>
            <dd>
                <ul>
                    <li>入居契約時の立会い、電気・ガス・水道など使用開始手続の援助</li>
                    <li>退去希望の確認、電気・ガス・水道などの使用中止手続の援助</li>
                </ul>
            </dd>
        </dl>
        <dl>
            <dt><span class="pink">2.</span>夜間の宿直体制</dt>
            <dd>
                <ul>
                    <li>夜間急変時の救急連絡、ご家族などへの対応</li>
                </ul>
            </dd>
        </dl>
        <dl>
            <dt><span class="pink">3.</span>1日6回の安否確認（居室3回、お食事時間3回）</dt>
            <dd>
                <ul>
                    <li>具合が悪くなっていないか</li>
                    <li>部屋もしくは館内で倒れていないか</li>
                    <li>急変時の救急連絡、ご家族などへの対応</li>
                </ul>
            </dd>
        </dl>
        <dl>
            <dt><span class="pink">4.</span>介護保険外の支援</dt>
            <dd>
                <ul>
                    <li>居室内における簡単な営繕業務（実費はお客様負担となります）</li>
                    <li>修理、交換などについての業者連絡</li>
                    <li>連携医療機関の紹介、調整</li>
                    <li>訪問理美容の受付、業者調整</li>
                </ul>
            </dd>
        </dl>
        <dl>
            <dt><span class="pink">5.</span>離設対応</dt>
            <dd>
                <ul>
                    <li>外出後、戻ってこられない場合のご家族への連絡、および警察への届け出</li>
                </ul>
            </dd>
        </dl>
        <dl>
            <dt><span class="pink">6.</span>生活状況、身体状況の確認</dt>
            <dd>
                <ul>
                    <li>身体状況、体調、病気について新たに注目する変化はないか<br>（既往歴、服薬情報、医療，看護サマリーなどを提出されている方）</li>
                    <li>衛生、清潔、安全が維持されているか<br>（維持できていないと判断される場合は、ご家族、ケアマネージャーへご連絡の上、対応をご相談させていただきます）</li>
                </ul>
            </dd>
        </dl>
        <dl>
            <dt><span class="pink">7.</span>相談対応</dt>
            <dd>
                <ul>
                    <li>ご入居者様やご家族からの各種ご相談対応、苦情対応</li>
                </ul>
            </dd>
        </dl>
    </div>
    <!-- wrapper -->
</section>
<!-- about -->

<section class="disease">
    <div class="wrapper">
        <h3 class="headline05">対応可能な疾病について</h3>
        <ul class="grid1 cf">
            <li>
                <dl>
                    <dt>認知症</dt>
                    <dd>○</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>人工肛門</dt>
                    <dd>○</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>在宅酸素</dt>
                    <dd>○</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>喀痰吸引</dt>
                    <dd>△</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>気管切開</dt>
                    <dd>△</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>ALS</dt>
                    <dd>○</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>人工透析</dt>
                    <dd>△</dd>
                </dl>
            </li>
            <?php if(is_pc()): ?>
        </ul>
        <ul class="grid2 cf">
            <?php endif; ?>
            <li>
                <dl>
                    <dt>糖尿病</dt>
                    <dd>△</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>IVH</dt>
                    <dd>△</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>尿バルーン</dt>
                    <dd>○</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>胃瘻</dt>
                    <dd>△</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>鼻腔経管</dt>
                    <dd>×</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>褥瘡</dt>
                    <dd>○</dd>
                </dl>
            </li>
            <li>
                <dl>
                    <dt>ペースメーカー</dt>
                    <dd>○</dd>
                </dl>
            </li>
        </ul>
        <p>△＝医療措置が必要な方の入居、ご本人様の症状によりましては医師の指示のもと協議させていただく場合もございますのであらかじめご了承ください。</p>
    </div>
    <!-- wapper -->

</section>
