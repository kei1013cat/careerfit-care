
<?php get_header(); ?>

<div id="title">
	<div class="wrapper">
		<?php $slug_name = basename(get_permalink()); ?>
		<h2><?=$post->post_title;?></h2>
	</div>
</div>
<!-- title -->
<div id="pagetitle">
<div class="bg">
	<h3>準備中</h3>
</div>
</div>


<div class="wrapper">
	<div id="main_contents" class="cf">
		<div id="contents" class="mr15">
			<section class="tac">
			<div class="box">
			このページは公開に向け準備中です。<br>お待たせして申し訳ありませんが今しばらくお待ち下さいますようお願い申し上げます。<br><br>
			<p class="linkbtn1">
			<a href="<?php bloginfo('url'); ?>">トップページへ戻る</a>
			</p>
				</div>
			</section>
		</div>
		<!-- contents -->

	</div>
	<!-- main_contents --> 
</div>
<!-- wrapper -->

<?php get_footer(); ?>
