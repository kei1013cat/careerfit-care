
<?php get_header(); ?>



<?php 
  $news_list_num = 5; // 新着情報の取得記事件数
?>



<section class="mainimage">
  <div id="my-slider" class="slider-pro">
    <div class="sp-slides">
      <div class="slider1 sp-slide"><img class="sp-image" src="<?php bloginfo('template_url'); ?>/images/slider01<?php mobile_img(); ?>.jpg?v=20180621"" />
      <h2 class="pc sp-layer sp-padding" data-horizontal="-16%" data-vertical="38%" data-width="96%" data-show-delay="400" data-hide-delay="600"><img src="<?php bloginfo('template_url'); ?>/images/main_msg01.png" alt="真心のこもった自立支援と介護サービスでやすらぎの暮らしをサポート"></h2>
      </div>
    </div>
  </div>
  <!-- center_text -->
  <div class="message sp">
  <h2>真心のこもった自立支援と介護サービスで</h2>
  <p>やすらぎの暮らしをサポート</p>
  </div>
<div class="bg"></div>
</section>


<section class="about">
	<div class="cf">
	  <div class="left"><img src="<?php bloginfo('template_url'); ?>/images/index_left_obi.png"></div>
	  <div class="center">
	    <h2><img src="<?php bloginfo('template_url'); ?>/images/index_about_h<?php mobile_img(); ?>.jpg" alt="キャリアフィットケアサービスの介護サービス"></h2>
	    <p>私たちは人生を積み重ねてこられた方々に、<br class="pc">
	 より幸せな生活を送っていただくことを願い、<br class="pc">個人の人格と尊厳をなによりも大切に、<br class="pc">
	  真心のこもった自立支援と介護サービスの提供を通して、<br class="pc">至幸へのお手伝いをさせていただきます。</p>
	  </div>
	  <!-- center -->
	  <div class="right"><img src="<?php bloginfo('template_url'); ?>/images/index_right_obi.png"></div>
	 </div>
</section>
<!-- about -->

<section class="menu">
  <div class="wrapper">
  <ul class="cf">
    <li>
      <a href="<?php bloginfo('url'); ?>/house/">
        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_menu_photo1.jpg" alt="サービス付き高齢者向け住宅 アゼリア館北広島"></p>
        <div class="box"><h3><span>サービス付き高齢者向け住宅</span>アゼリア館北広島</h3></div>
      </a>
    </li>
    <li>
      <a href="<?php bloginfo('url'); ?>/visitcare/">
        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_menu_photo2.jpg?v=20180621" alt="創成川イースト ヘルパーステーション"></p>
        <div class="box"><h3><span>創成川イースト</span>ヘルパーステーション</h3></div>
      </a>
    </li>
    <li>
      <a href="<?php bloginfo('url'); ?>/careplan/">
        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_menu_photo3.jpg?v=20180621" alt="創成川イースト　ケアプランセンター"></p>
        <div class="box"><h3><span>創成川イースト</span>ケアプランセンター</h3></div>
      </a>
    </li>
    <li>
      <a href="<?php bloginfo('url'); ?>/healthcare/">
        <p class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_menu_photo4.jpg?v=20180621" alt="総合ヘルスケア"></p>
        <div class="box one"><h3>総合ヘルスケア</h3></div>
      </a>
    </li>
  </ul>
  </div>
  <!-- wrapper -->
</section>
<!-- menu -->


<?php $newslist1 = get_posts( array( 'category_name' => 'お知らせ','posts_per_page' => $news_list_num )); ?>
<?php $newslist3 = get_posts( array( 'category_name' => '創成川イースト','posts_per_page' => $news_list_num )); ?>

<section class="news" id="news">
<h2 class="headline03">新着情報</h2>
<div class="wrapper">
<ul class="tab">
      <li class="select"><a href="javascript:void(0)">お知らせ</a></li>
      <li><a href="javascript:void(0)">アゼリア館北広島</a></li>
      <li><a href="javascript:void(0)">創成川イースト</a></li>
</ul>

<?php require_once('rss_fetch.inc'); ?>
<?php
$url = 'http://azaleakan.jugem.jp/?mode=rss';
$rss = fetch_feed($url);

if (!is_wp_error( $rss ) ) : // ちゃんとフィードが生成されているかをチェックします。
    // すべてのフィードから最新５件を出力します。
    $maxitems = $rss->get_item_quantity($news_list_num); 
    // 0件から始めて指定した件数までの配列を生成します。
    $rss_items = $rss->get_items(0, $maxitems); 
endif;
?>

<ul class="content">
    <li>
      <?php
          foreach( $newslist1 as $post ):
          setup_postdata( $post );
      ?>
      <dl>
        <dt><?php the_time('Y年n月j日'); ?></dt>
        <dd><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
        <?php if($post==end($newslist1)): ?>
          <p class="linkbtn3"><a href="<?php bloginfo('url'); ?>/newslist/?cat_name=お知らせ">一覧へ</a></p>
        <?php endif; ?>
        </dd>
      </dl> 
      <?php
        endforeach;
        wp_reset_postdata();
      ?>
    </li>
    <li class="hide">


        <?php
        foreach ( $rss_items as $item ) : ?>
        <?php $f_date = $item->get_date('Y年m月d日'); ?>
        <dl>
          <dt><?php echo $f_date; ?></dt>
          <dd>
            <a href='<?php echo $item->get_permalink(); ?>'
            target="_blank" title='<?php echo 'Posted '.$item->get_date('j F Y | g:i a'); ?>'>
            <?php echo $item->get_title(); ?>
            </a>
            <?php if($item==end($rss_items)): ?>
              <p class="linkbtn3"><a href="<?php bloginfo('url'); ?>/house/diary/">一覧へ</a></p>
            <?php endif; ?>
          </dd>
        </dl>
        <?php endforeach; ?>


    </li>
    <li class="hide">
      <?php
          foreach( $newslist3 as $post ):
          setup_postdata( $post );
      ?>
      <dl>
        <dt><?php the_time('Y年n月j日'); ?></dt>
        <dd><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
        <?php if($post==end($newslist3)): ?>
          <p class="linkbtn3"><a href="<?php bloginfo('url'); ?>/newslist/?cat_name=創成川イースト">一覧へ</a></p>
        <?php endif; ?>
        </dd>
      </dl> 
      <?php
        endforeach;
        wp_reset_postdata();
      ?>
    </li>
</ul>
</div>
<!-- wrapper -->
</section>


  </div>
  <!--contents -->

<!-- contents -->
<?php get_footer(); ?>