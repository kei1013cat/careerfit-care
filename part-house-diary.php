<?php
/*
Template Name: newslist
*/
?>

<?php require_once('rss_fetch.inc'); ?>
<?php
$url = 'http://azaleakan.jugem.jp/?mode=rss';
$rss = fetch_feed($url);

if (!is_wp_error( $rss ) ) : // ちゃんとフィードが生成されているかをチェックします。
    // すべてのフィードから最新５件を出力します。
    $maxitems = $rss->get_item_quantity(20); 
    // 0件から始めて指定した件数までの配列を生成します。
    $rss_items = $rss->get_items(0, $maxitems); 
endif;
?>

		<section class="diary bg_yellow">
		<h3>アゼリア日記（お知らせ）</h3>
		<div class="wrapper910">
        <?php
        foreach ( $rss_items as $item ) : 
       	?>
        <?php $f_date = $item->get_date('Y年m月d日'); ?>
        <dl class="cf newslist">
          <dt><?php echo $f_date; ?></dt>
          <dd>
            <a href='<?php echo $item->get_permalink(); ?>'
            target="_blank" title='<?php echo 'Posted '.$item->get_date('j F Y | g:i a'); ?>'>
            <?php echo $item->get_title(); ?>
            </a>
            <?php if($item==end($rss_items)): ?>
              <p class="linkbtn3"><a href="http://azaleakan.jugem.jp/" target="_blank">もっと見る</a></p>
            <?php endif; ?>
          </dd>

       </dl>

        <?php endforeach; ?>





			</div>
		</section>













		<?php wp_reset_query(); ?>
