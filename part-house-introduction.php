<?php // 共通CSSクラス　?>

<section class="pagelink_col3">
	<ul class="cf">
		<li><a href="#01" class="link">居室タイプ</a></li>
		<li><a href="#02" class="link">居室Aタイプ</a></li>
		<li><a href="#03" class="link">居室Bタイプ</a></li>
		<li><a href="#04" class="link">居室Cタイプ</a></li>
		<li><a href="#05" class="link">レストラン</a></li>
		<li><a href="#06" class="link">館内の様子</a></li>
	</ul>
</section>

<section class="roomtype">
	<div class="wrapper"  id="01">
	<h2 class="headline02">居室タイプ<span class="line"></span></h2>
	<ul class="cf">
		<li><dl class="cf col1">
			<dt>Aタイプ<span>１ルーム（10室）</span></dt>
			<dd>
				<p>単身向けのひろびろ設計タイプ<br class="pc"><br class="pc"></p>
				<p class="area">専有面積25.025㎡（16.2帖）</p>
				<img src="<?php bloginfo('template_url'); ?>/images/house_shoukai_atype.jpg" alt="居室Aタイプ １ルーム（10室）" />
			</dd>
		</dl></li>

		<li><dl class="cf col2">
			<dt>Bタイプ<span>１LDK（30室）</span></dt>
			<dd>
				<p>ご夫婦、親子、ご兄弟での<br>ご利用も可能な２部屋タイプ</p>
				<p class="area">専有面積37.395㎡（24.1帖）</p>
				<img src="<?php bloginfo('template_url'); ?>/images/house_shoukai_btype.jpg" alt="居室Bタイプ １LDK（30室）" />
			</dd>
		</dl></li>

		<li><dl class="cf col3">
			<dt>Cタイプ<span>２LDK（4室）</span></dt>
			<dd>
				<p>1ランク上の広さを確保した<br>ゆとりの２LDKタイプ</p>
				<p class="area">専有面積50.275㎡（32.5帖）</p>
				<img src="<?php bloginfo('template_url'); ?>/images/house_shoukai_ctype.jpg" alt="居室Cタイプ １ルーム（10室）" />
			</dd>
		</dl></li>
	</ul>
	<div class="grid">
		<h3 class="headline05">各戸共通設備</h3>
		<p>トイレ（ウォシュレット付）・洗面台・キッチン・温水器・TV視聴設備・クローゼット・下駄箱・玄関椅子（収納式）・各部手すり・トランクルーム・暖房設備・換気設備・照明設備</p>
	</div>
	<!-- grid -->

	<div class="grid">
		<h3 class="headline05">B・Cタイプ設備</h3>
		<p>ユニットバス・洗濯水洗／排水　※エアコンは各室設置可</p>
	</div>
	<!-- grid -->

	</div>
	<!-- wrapper -->
</section>
<!-- roomtype -->

<section class="roomtype_detail">
	<div class="grid-tail col4 bg_gray">
	<div class="wrapper"  id="02">
		<h3 class="headline02">居室Aタイプ<span class="line"></span></h3>
		<h4>お一人でお住まいになる方向けの広々設計1ルームです。</h4>
		<ul class="cf">
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo1.jpg" data-lightbox="group1" data-title="居室Aタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo1.jpg" alt="居室Aタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo2.jpg" data-lightbox="group1" data-title="居室Aタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo2.jpg" alt="居室Aタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo3.jpg" data-lightbox="group1" data-title="居室Aタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo3.jpg" alt="居室Aタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo4.jpg" data-lightbox="group1" data-title="居室Aタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo4.jpg" alt="居室Aタイプ" /></a></li>
		</ul>
	</div>
	<!-- wrapper -->
	</div>
	<!-- grid -->

	<div class="grid-tail col4">
		<div class="wrapper" id="03">
		<h3 class="headline02">居室Bタイプ<span class="line"></span></h3>
		<h4>ご夫婦はもちろん、親子・ご兄弟・ご親戚でのご利用も可能な2部屋タイプです。</h4>
		<ul class="cf">
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo5.jpg" data-lightbox="group2" data-title="居室Bタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo5.jpg" alt="居室Bタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo6.jpg" data-lightbox="group2" data-title="居室Bタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo6.jpg" alt="居室Bタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo7.jpg" data-lightbox="group2" data-title="居室Bタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo7.jpg" alt="居室Bタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo8.jpg" data-lightbox="group2" data-title="居室Bタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo8.jpg" alt="居室Bタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo9.jpg" data-lightbox="group2" data-title="居室Bタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo9.jpg" alt="居室Bタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo10.jpg" data-lightbox="group2" data-title="居室Bタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo10.jpg" alt="居室Bタイプ" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo11.jpg" data-lightbox="group2" data-title="居室Bタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo11.jpg" alt="居室Bタイプ" /></a></li>
		</ul>
	</div>
	<!-- warpper -->
	</div>
	<!-- grid -->

	<div class="grid-tail col3 bg_gray">
		<div class="wrapper" id="04">
		<h3 class="headline02">居室Cタイプ<span class="line"></span></h3>
		<h4>1ランク上の広さを確保した、ゆとり溢れる2LDKタイプです。</h4>
		<ul class="cf">

			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo12.jpg" data-lightbox="group3" data-title="居室Cタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo12.jpg" alt="居室Cタイプ"  /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo13.jpg" data-lightbox="group3" data-title="居室Cタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo13.jpg" alt="居室Cタイプ"  /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo14.jpg" data-lightbox="group3" data-title="居室Cタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo14.jpg" alt="居室Cタイプ"  /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo15.jpg" data-lightbox="group3" data-title="居室Cタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo15.jpg" alt="居室Cタイプ"  /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo16.jpg" data-lightbox="group3" data-title="居室Cタイプ"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo16.jpg" alt="居室Cタイプ"  /></a></li>
		</ul>
	</div>
	<!-- wrapper -->
	</div>
	<!-- grid -->

	<div class="grid-tail col3">
	<div class="wrapper" id="05">
		<h3 class="headline02">レストラン<span class="line"></span></h3>
		<h4>笑顔の集うレストラン。栄養バランスはもちろん、なにより美味しく楽しい食事<br class="pc">を目指し、道産・国産食材を中心に献立を組み立てて います。またご飯/パン、<br class="pc">肉/魚ほか、和・洋・中の調理法をお選びいただけるよう工夫しています。</h4>
		<ul class="cf">
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo17.jpg" data-lightbox="group4" data-title="レストラン"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo17.jpg" alt="レストラン" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo18.jpg" data-lightbox="group4" data-title="レストラン"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo18.jpg" alt="レストラン" /></a></li>
			<li><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo19.jpg" data-lightbox="group4" data-title="レストラン"><img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo19.jpg" alt="レストラン" /></a></li>
		</ul>
	</div>
	<!-- wrapper -->
	</div>
	<!-- grid -->

	<div class="grid-tail col3 bg_gray">
		<div class="wrapper" id="06">
		<h3 class="headline02">館内の様子<span class="line"></span></h3>
		<h4>明るく開放的な館内。エレベーターホールのロビーや中庭、デイサービス/訪問<br class="pc">介護ステーション屋上など、集い語らうゆとりのスペースが用意されています。</h4>
		<ul class="cf">
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo20.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo20.jpg" alt="館内の様子" />
				</a>
				<p class="text">1Fエントランスから入ると左手に事務所があります。</p>
			</li>
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo21.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo21.jpg" alt="館内の様子" />
				</a>
				<p class="text">落ち着いた雰囲気のロビー。会話もはずみます。</p>
			</li>
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo22.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo22.jpg" alt="館内の様子" />
				</a>
				<p class="text">エレベーターは階ごとに色分けされています。</p>
			</li>
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo23.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo23.jpg" alt="館内の様子" />
				</a>
				<p class="text">爽やかな風が駆け抜ける中庭。特に夏場の人気スポットです。</p>
			</li>
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo24.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo24.jpg" alt="館内の様子" />
				</a>
				<p class="text">中庭を上から見た光景。この吹き抜けと渡り廊下が開放感を生みます。</p>
			</li>
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo25.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo25.jpg" alt="館内の様子" />
				</a>
				<p class="text">廊下も陽光に溢れて明るい雰囲気。もちろん車椅子にも優しい設計です。</p>
			</li>
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo26.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo26.jpg" alt="館内の様子" />
				</a>
				<p class="text">3階居室の窓から見た風景。大きな空の下、ゆったりと時が流れます。</p>
			</li>
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo27.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo27.jpg" alt="館内の様子" />
				</a>
				<p class="text">デイサービス/訪問介護の屋上に出ることができます。</p>
			</li>
			<li class="matchHeight"><a href="<?php bloginfo('template_url'); ?>/images/kitahouse_l_photo28.jpg" data-lightbox="group5" data-title="館内の様子">
				<img src="<?php bloginfo('template_url'); ?>/images/kitahouse_photo28.jpg" alt="館内の様子" />
				</a>
				<p class="text">共用ランドリーも最新の機器を完備しています。</p>
			</li>
		</ul>
	</div>
	<!-- wrapper -->
	</div>
	<!-- grid -->

</section>
<!-- roomtype_detail -->
